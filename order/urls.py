from django.urls import path

from order.apps import OrderConfig
from order.views import CreateOrderView, get_successful_payment_url

app_name = OrderConfig.name

urlpatterns = [
    path('order/create', CreateOrderView.as_view(), name='create_order'),
    path('order/successful_payment', get_successful_payment_url, name='successful_payment'),
]
